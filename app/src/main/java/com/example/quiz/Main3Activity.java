package com.example.quiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);
    }

    TextView tv = findViewById(R.id.textGrate);
    Boolean otvet = getIntent().getBooleanExtra("OTVET", false);
    if (otvet {
        tv.setText("ПРАВИЛЬНО");
    } else{
        tv.setText("NE ПРАВИЛЬНО");
    }
}
