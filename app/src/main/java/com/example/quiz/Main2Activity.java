package com.example.quiz;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }

    public void pravilno(View view) {
        Intent intent = new Intent(this, Main3Activity.class); // намерение перейти

        intent.putExtra("OTVET", true);

        startActivity(intent);
    }

    public void nepravilno(View view) {
        Intent intent = new Intent(this, Main3Activity.class); // намерение перейти

        intent.putExtra("OTVET", false);

        startActivity(intent);
    }
}
